import React from 'react';
import './Header.css';
import {LinkContainer} from "react-router-bootstrap";
import {Button, Nav, Navbar} from "react-bootstrap";

const Header = () => {
    return (
        <Navbar inverse>
            <Navbar.Header>
                <Navbar.Brand>
                    Quotes Central
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav pullRight className='Main-Page-Buttons'>
                    <LinkContainer to='/' exact><Button bsStyle="primary" className="MainPage-Button">All quotes</Button></LinkContainer>
                    <LinkContainer to='/submitNewQuote'><Button bsStyle="primary">Submit new quote</Button></LinkContainer>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default Header;