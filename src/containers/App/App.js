import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MainPage from "../MainPage/Quotes";
import Header from "../../Header/Header";
import SubmitNewQuote from "../../SubmitNewQuoute/SubmitNewQuote";
import EditQuote from "../../components/EditQuote/EditQuote";

class App extends Component {
  render() {
    return (
          <div className="App">
              <Header/>
          <Switch>
              <Route path='/' exact component={MainPage}/>
              <Route path='/submitNewQuote' exact component={SubmitNewQuote} />
              <Route path='/post/:id' component={EditQuote} />

          </Switch>
          </div>
    );
  }
}

export default App;
