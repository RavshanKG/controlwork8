import React from 'react';
import axios from 'axios';
import Quote from "../../components/Quote/Quote";
import MainNav from "../../components/MainNav/MainNav";

class Quotes extends React.Component {
    state = {
        quotesList: []
    };

    pushQuoteToState = (value, id) => {
        const quotesList = [...this.state.quotesList];
        quotesList.push({'data': value, 'id': id});
        this.setState({quotesList})
    }



    componentDidMount() {
        axios.get('/post.json').then(response => {
            console.log(response);
            for (let key in response.data){
                this.pushQuoteToState(response.data[key], key)
            }
        })
    }

    deleteQuote = id => {
        axios.delete(`/post/${id}.json`).then(() => {
            this.setState(prevState => {
                const quotes = [...prevState.quotesList];
                const index = quotes.findIndex((quote) => quote.id === id);
                quotes.splice(index, 1);
                return {quotesList: quotes};
            })
        })
    }


 render () {
        console.log(this.state.quotesList);
     return (
         <div>
             <MainNav/>
            <div className="container">
            {this.state.quotesList.map(value => <Quote
                category={value.data.category}
                author={value.data.author}
                quoteText={value.data.quoteText}
                id={value.id}
                clicked={() => this.deleteQuote(value.id)}/>)}
            </div>
         </div>
     )
 }
}

export default Quotes;

