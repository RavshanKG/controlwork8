import React from 'react';
import {Button, ControlLabel, FormControl, FormGroup, PageHeader} from "react-bootstrap";
import axios from 'axios';


class SubmitNewQuote extends React.Component {
    state = {
        category: '',
        author: '',
        quoteText: ''
    };

    SubmitNewQuote = event => {
        event.preventDefault();
        axios.post('/post.json', {'category': this.state.category, 'author': this.state.author, 'quoteText': this.state.quoteText}).then((response) => {
            this.props.history.push('/');
        })
    };
    HandleChange = event => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render () {
        console.log(this.state.category);
        return (
            <div className="container">
                <form onSubmit={this.SubmitNewQuote}>
                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Category</ControlLabel>
                        <FormControl name="category" value={this.state.category} onChange={this.HandleChange} componentClass="select" placeholder="select">
                            <option >Famous people</option>
                            <option >Humour</option>
                            <option >Saying</option>
                            <option >Motivational Quotes</option>
                            <option >Other</option>
                        </FormControl>
                    </FormGroup>
                    <FormGroup controlId="formControlsSelectMultiple">
                        <ControlLabel>Author</ControlLabel>
                        <FormControl name="author" value={this.state.author} onChange={this.HandleChange} type="text">
                        </FormControl>
                    </FormGroup>
                    <FormGroup controlId="formControlsTextarea">
                        <ControlLabel>Quote text</ControlLabel>
                        <FormControl name="quoteText" value={this.state.quoteText} onChange={this.HandleChange} componentClass="textarea" placeholder="Quote" />
                    </FormGroup>
                    <Button type={"submit"}>Submit</Button>
                </form>
            </div>
        )
    }
}

export default SubmitNewQuote;