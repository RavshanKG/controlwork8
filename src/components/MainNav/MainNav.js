import React from 'react';
import './MainNav.css';
import {Button, ButtonGroup} from "react-bootstrap";
import {Link} from "react-router-dom";

const MainNav = props => {
    return (
        <div className="container">
            <ButtonGroup className="MainNav-Buttons">
                <h4>Categories</h4>
                <Button><Link to="/famous-people" >Famous people</Link></Button>
                <Button><Link to="/humour" >Humour</Link></Button>
                <Button><Link to="/saying" >Saying</Link></Button>
                <Button><Link to="/motivational-quotes" >Motivational Quotes</Link></Button>
                <Button><Link to="/other" >Other</Link></Button>
            </ButtonGroup>
        </div>
    )
}
export default MainNav;