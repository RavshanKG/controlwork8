import React from 'react';
import axios from 'axios';
import {Button, ControlLabel, FormControl, FormGroup, Panel} from "react-bootstrap";
import SubmitNewQuote from "../../SubmitNewQuoute/SubmitNewQuote";

class EditQuote extends React.Component {
    state = {
            category: '',
            author: '',
            quoteText: '',
        loadedQuote: null
    };

    componentDidMount () {
        const id = this.props.match.params.id;

        console.log(id);

        axios.get(`/post/${id}.json`).then(response => {
            this.setState({loadedQuote: response.data});
            console.log(response.data)

        })

    }

    EditQuote = event => {
        event.preventDefault();
        const id = this.props.match.params.id;
        console.log(id)

        axios.put(`/post/${id}.json`, {'category': this.state.category, 'author': this.state.author, 'quoteText': this.state.quoteText}).then((response) => {
            this.props.history.push('/');
        })
    };
    HandleChange = event => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render () {
        if (this.state.loadedQuote) {
            return (
                <div className="container">
                    <form onSubmit={this.EditQuote}>
                        <FormGroup controlId="formControlsSelect">
                            <ControlLabel>Category</ControlLabel>
                            <FormControl name="category" value={this.state.category} onChange={this.HandleChange} componentClass="select" placeholder="select">
                                <option >Famous people</option>
                                <option >Humour</option>
                                <option >Saying</option>
                                <option >Motivational Quotes</option>
                                <option >Other</option>
                            </FormControl>
                        </FormGroup>
                        <FormGroup controlId="formControlsSelectMultiple">
                            <ControlLabel>Author</ControlLabel>
                            <FormControl name="author" value={this.state.author} onChange={this.HandleChange} type="text">
                            </FormControl>
                        </FormGroup>
                        <FormGroup controlId="formControlsTextarea">
                            <ControlLabel>Quote text</ControlLabel>
                            <FormControl name="quoteText" value={this.state.quoteText} onChange={this.HandleChange} componentClass="textarea" placeholder="Quote" />
                        </FormGroup>
                        <Button type={"submit"}>Submit</Button>
                    </form>
                </div>
        );
    } else {
        return <p>Loading...</p>}
    }
}

export default EditQuote;