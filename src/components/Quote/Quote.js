import React from 'react';
import './Quote.css';
import {Button, Panel} from "react-bootstrap";
import {NavLink} from "react-router-dom";

const Quote = props => {
    return (
        <div className="container">
            <Panel bsStyle="primary">
                <Button className="Delete-Button" onClick={props.clicked}>Delete quote</Button>
                <Panel.Body><NavLink to={'post/' + props.id}>{props.author}</NavLink></Panel.Body>
                <Panel.Footer>{props.quoteText}</Panel.Footer>
            </Panel>
        </div>
    )
}
export default Quote;